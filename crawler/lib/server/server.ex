defmodule Server do
  use Application

  def start(_type, _args) do
    IO.puts("Starting server...")
    children = [Plug.Cowboy.child_spec(scheme: :http, plug: Server.Router, options: [port: 8080])]
    opts = [strategy: :one_for_one, name: Server.Supervisor]
    Supervisor.start_link(children, opts) |> IO.inspect
  end
end
