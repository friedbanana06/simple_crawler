defmodule Server.Router do
  use Plug.Router
  use Plug.Debugger

  require Logger

  plug CORSPlug

  plug(Plug.Logger, log: :debug)

  plug(:match)

  plug(:dispatch)

  # Define routes

  get "/api/categories" do
    categories = CategoriesStore.get()
    { status, result } = JSON.encode(categories)
    IO.inspect(status)
    send_resp(conn, 200, result)
  end

  get "/api/products" do
    qps = Plug.Conn.fetch_query_params(conn)
    categoryId = Map.get(qps.query_params, "categoryId", "0")
    IO.inspect(categoryId)
    products = ProductsStore.get(categoryId)
    { status, result } = JSON.encode(products)
    IO.inspect(status)
    send_resp(conn, 200, result)
  end

  match _ do
    send_resp(conn, 404, "not found!")
  end
end
