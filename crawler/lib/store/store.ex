defmodule CategoriesStore do
  use Agent
  def start_link(initial_value) do
    Agent.start_link(fn -> initial_value end, name: __MODULE__)
  end

  def get() do
    Agent.get(__MODULE__, fn state -> state end)
  end

  def set(values) do
    Agent.update(__MODULE__, fn state -> state = values end)
  end
end

defmodule ProductsStore do
  use Agent

  def start_link(initial_value) do
    Agent.start_link(fn -> initial_value end, name: __MODULE__)
  end

  def get(categoryId) do
    Agent.get(__MODULE__, fn state -> Map.get(state, categoryId, []) end)
  end

  def set(categoryId, products) do
    Agent.update(__MODULE__, fn state -> Map.put(state, categoryId, products) end)
  end
end

# def ProductsStore do

# end
