defmodule Crawl.Scheduler do
  use GenServer

  @impl true
  def start_link do
    GenServer.start_link(__MODULE__, %{})
  end

  @impl true
  def init(state) do
    :timer.send_interval(120_000, :work)
    {:ok, state}
  end

  @impl true
  def handle_info(:work, state) do
    doTask(state)
    {:noreply, state}
  end

  defp doTask(_state) do
    IO.puts("Interval called...\n")
    Crawly.Engine.stop_spider(Spider)
    Crawly.Engine.start_spider(Spider)
  end

end

defmodule Crawler do
  def start() do
    Crawl.Scheduler.start_link()
    children = [Crawl.Scheduler]
    Supervisor.start_link(children, strategy: :one_for_one)
  end
end
