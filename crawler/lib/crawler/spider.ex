defmodule Spider do

  require Logger
  import Meeseeks.CSS

  @behaviour Crawly.Spider

  @impl Crawly.Spider
  def base_url(), do: "https://shopee.vn"

  @impl Crawly.Spider
  def init() do
    [
      start_urls: [
        "https://shopee.vn/apple_flagship_store"
      ]
    ]
  end

  @impl Crawly.Spider
  def parse_item(response) do
    case String.contains?(response.request_url, "apple_flagship_store") do
      true ->
        parse_categories(response)
      false ->
        parse_products(response)
    end
  end

  defp parse_categories(response) do
    IO.inspect("parse_categories called")

    categories = Meeseeks.parse(response.body, :html)
      |> Meeseeks.all(css("div.navbar-with-more-menu__items a"))
      |> Enum.filter(fn categoryItem -> String.contains?(Meeseeks.attr(categoryItem, "href"), "shop") end)
      |> Enum.map(fn categoryItem ->
        categoryName = Meeseeks.text(categoryItem)
        href = Meeseeks.attr(categoryItem, "href")
        if String.contains?(href, "shopCollection=") do
          %{id: Enum.at(String.split(href, "shopCollection="), 1), name: categoryName, href: href}
        else
          %{id: "0", name: categoryName, href: href}
        end
      end)

    IO.inspect(categories)
    if length(categories) > 0 do
      CategoriesStore.set(categories)
      requests = categories
        |> Enum.map(fn category -> category.href end)
        |> Crawly.Utils.build_absolute_urls(base_url())
        |> Crawly.Utils.requests_from_urls()
      %Crawly.ParsedItem{requests: requests}
    else
      IO.inspect("empty_categories")
      %Crawly.ParsedItem{requests: []}
    end

  end

  defp parse_products(response) do
    requestURL = response.request_url
    IO.inspect("parse_products called (" <> requestURL <> ")")

    case Meeseeks.fetch_all(Meeseeks.parse(response.body, :html), css("div.shop-search-result-view__item")) do
        { :ok, productItems } ->
          products = Enum.map(productItems, fn productItem ->
            %{
                thumbnail: Meeseeks.attr(Meeseeks.one(productItem, css("img.mxM4vG")), "src"),
                name: Meeseeks.text(Meeseeks.one(productItem, css("div.yQmmFK"))),
                oldPrice: Meeseeks.text(Meeseeks.one(productItem, css("div.WTFwws._3f05Zc._3_-SiN"))),
                newPrice: Meeseeks.all(productItem, css("div.WTFwws._1lK1eK._5W0f35 span"))
                  |> Enum.map(fn a -> Meeseeks.text(a) end)
                  |> Enum.reduce("", fn next, curr -> curr <> next end)
             }
          end)

          IO.inspect(products)
          if String.contains?(requestURL, "shopCollection=") do
            ProductsStore.set(Enum.at(String.split(requestURL, "shopCollection="), 1), products)
          else
            ProductsStore.set("0", products)
          end

        { :error, %Meeseeks.Error{type: :select, reason: :no_match} } ->
          IO.inspect("product_items_not_found")
    end

    # TODO: Handle paginition

    %Crawly.ParsedItem{items: [], requests: []}
  end

end
