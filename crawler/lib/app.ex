defmodule App do

  def start(type, args) do
    CategoriesStore.start_link([])
    ProductsStore.start_link(%{})

    Crawler.start()
    Server.start(type, args)

    # FIXME: Put this line makes prog not run -_-
    # IO.puts("Everything will be okayy.. :D\n")
  end

end
