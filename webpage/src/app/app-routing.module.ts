import { NgModule } from '@angular/core';
import { ExtraOptions, RouterModule, Routes } from '@angular/router';
//
import { HomeComponent } from './home/home.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';


const routes: Routes = [
	{ path: 'home', component: HomeComponent },
	{ path: '', redirectTo: 'home', pathMatch: 'full' },
	{ path: '**', component: PageNotFoundComponent }
];

const routeOptions: ExtraOptions = {
	enableTracing: false,
	relativeLinkResolution: 'legacy'
};

@NgModule({
	imports: [RouterModule.forRoot(routes, routeOptions)],
	exports: [RouterModule]
})
export class AppRoutingModule {
}
