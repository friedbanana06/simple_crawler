export class Product {
	constructor(
		public name: string,
		public thumbnail: string,
		public oldPrice: string,
		public newPrice: string
	) {
	}
}
