import { Component, OnInit } from '@angular/core';
import { Category } from '../shared/category.model';
import { Product } from '../shared/product.model';
import { HttpClient, HttpHandler, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';
import { finalize } from 'rxjs/operators';
import { NotifierService } from 'angular-notifier';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss'],
	providers: [HttpClient]
})
export class HomeComponent implements OnInit {
	categories: Category[] = [];
	selectedCategory: string | null = null;
	products: Product[] | null = null;
	serverURL = window.location.host + ':' + window.location.port;

	constructor(
		private httClient: HttpClient,
		private spinner: NgxSpinnerService,
		private notifier: NotifierService
	) {
	}

	ngOnInit() {
		this.spinner.show().then(() => {
			this.fetchCategories()
				.subscribe(res => {
					if (res && res.body) {
						this.categories = res.body;
					}
					if (this.categories.length > 0) {
						this.selectedCategory = this.categories[0].id;
						this.getProducts();
					} else {
						this.spinner.hide().then();
					}
				}, () => {
					this.spinner.hide().then(() => {
						this.notifier.notify('error', 'Đã có lỗi xảy ra. Vui lòng thử lại sau!');
					});
				});
		});
	}

	getProducts(): void {
		this.spinner.show().then(() => {
			this.fetchProducts(this.selectedCategory)
				.pipe(finalize(() => this.spinner.hide().then()))
				.subscribe(res => {
					if (res && res.body) {
						this.products = res.body;
					}
				}, () => {
					this.notifier.notify('error', 'Đã có lỗi xảy ra. Vui lòng thử lại sau!');
				});
		});
	}

	fetchCategories(): Observable<HttpResponse<Category[]>> {
		return this.httClient.get<Category[]>(`http://${this.serverURL}/api/categories`, { observe: 'response' });
	}

	fetchProducts(categoryId: string): Observable<HttpResponse<Product[]>> {
		const params = new HttpParams().set('categoryId', categoryId);
		return this.httClient.get<Product[]>(`http://${this.serverURL}/api/products`, { params, observe: 'response' });
	}

}
